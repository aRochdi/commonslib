//
//  File.swift
//  
//
//  Created by Rochdi Aries on 22/10/2021.
//

import UIKit

// MARK: Protocol

public protocol Coordinator: AnyObject {
    var mainViewController: UINavigationController! { get }
    var childCoordinators: [Coordinator] { get set }
    
    func present(childCoordinator coordinator: Coordinator, animated: Bool, completion: (() -> Void)?)
    func dismissChildCoordinator(animated: Bool, completion: (() -> Void)?)
    func push(childCoordinator: Coordinator, animated: Bool)
    func popChildCoordinator(animated: Bool)
}

// MARK: Default Implementation

public extension Coordinator {
    func present(childCoordinator coordinator: Coordinator, animated: Bool = true, completion: (() -> Void)? = nil) {
        childCoordinators.append(coordinator)
        self.mainViewController.present(coordinator.mainViewController, animated: animated, completion: completion)
    }
    func dismissChildCoordinator(animated: Bool = true, completion: (() -> Void)? = nil) {
        self.mainViewController.dismiss(animated: animated, completion: completion)
        _ = childCoordinators.popLast()
    }
    
    func push(childCoordinator: Coordinator, animated: Bool) {
        self.childCoordinators.append(childCoordinator)
        guard let rootCtrl = childCoordinator.mainViewController.viewControllers.first else {
            return
        }
        
        self.mainViewController.pushViewController(rootCtrl, animated: animated)
    }
    func popChildCoordinator(animated: Bool = true) {
        self.mainViewController.popViewController(animated: animated)
         _ = childCoordinators.popLast()
    }
}
