//
//  File.swift
//  
//
//  Created by Rochdi Aries on 22/10/2021.
//

import Foundation

public class Observable<T> {
    
    public typealias ObserverClosure = (_ value: T) -> Void
    
    // MARK: - properties
    
    private var observer: ObserverClosure?
    
    // MARK: - Life Cycle
    
    public init(value: T) {
        self.value = value
    }
    
    public var value: T {
        didSet {
            DispatchQueue.main.async {
                self.observer?(self.value)
            }
        }
    }
    
    // MARK: - Public API
    
    public func subscribe (observerClosure: @escaping ObserverClosure) {
        self.observer = observerClosure
        observerClosure(self.value)
    }
    
    public func subscribeToNext (observerClosure: @escaping ObserverClosure) {
        self.observer = observerClosure
    }
} 
